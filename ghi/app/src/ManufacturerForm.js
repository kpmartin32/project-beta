import React, { useState } from "react";

function ManufacturerForm() {
  const [manufacturerData, setManufacturerData] = useState({
    name: "",
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setManufacturerData({
      ...manufacturerData,
      [name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    fetch("/api/manufacturers/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(manufacturerData),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Manufacturer created:", data);
        setManufacturerData({
          name: "",
        });
      })
      .catch((error) => {
        console.error("Error creating manufacturer:", error);
      });
  };

  return (
    <div>
      <h2>Create a New Manufacturer</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="name">Name:</label>
          <input
            type="text"
            id="name"
            name="name"
            value={manufacturerData.name}
            onChange={handleChange}
            required
          />
        </div>
        <div>
          <button type="submit">Create Manufacturer</button>
        </div>
      </form>
    </div>
  );
}

export default ManufacturerForm;
