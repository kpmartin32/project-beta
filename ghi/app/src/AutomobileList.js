import React, { useState, useEffect } from "react";

function AutomobileList() {
  const [automobiles, setAutomobiles] = useState([]);

  useEffect(() => {
    fetch("/api/automobiles/")
      .then((response) => response.json())
      .then((data) => setAutomobiles(data.autos))
      .catch((error) => console.error("Error fetching automobiles", error));
  }, []);

  return (
    <div>
      <h1>Automobile Inventory</h1>
      <table>
        <thead>
          <tr>
            <th>Color</th>
            <th>Year</th>
            <th>VIN</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map((auto) => (
            <tr key={auto.vin}>
              <td>{auto.color}</td>
              <td>{auto.year}</td>
              <td>{auto.vin}</td>
              <td>{auto.sold ? "Yes" : "No"}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AutomobileList;
