import React, { useState, useEffect } from 'react';

function AppointmentsForm() {
  const [vin, setVin] = useState('');
  const [customerName, setCustomerName] = useState('');
  const [dateTime, setDateTime] = useState('');
  const [technicianId, setTechnicianId] = useState('');
  const [reason, setReason] = useState('');
  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    const fetchTechnicians = async () => {
      try {
        const response = await fetch('http://localhost:8080/api/technicians/');
        if (!response.ok) {
          throw new Error('Failed to fetch technicians');
        }
        const data = await response.json();
        setTechnicians(data.technicians);
      } catch (error) {
        console.error('Error fetching technicians:', error.message);
      }
    };

    fetchTechnicians();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const appointmentData = {
      vin,
      customer: customerName,
      date_time: dateTime,
      technician: technicianId,
      reason,
    };

    try {
      const response = await fetch('http://localhost:8080/api/appointments/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(appointmentData),
      });

      if (response.ok) {
        const data = await response.json();
        console.log('Appointment created successfully!', data);
        setVin('');
        setCustomerName('');
        setDateTime('');
        setTechnicianId('');
        setReason('');
      } else {
        console.error('Failed to create appointment.');
      }
    } catch (error) {
      console.error('An error occurred:', error);
    }
  };

  return (
    <div className="appointment-form">
      <h2>Create a service appointment</h2>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="vin" className="form-label">Automobile VIN</label>
          <input
            type="text"
            id="vin"
            className="form-control"
            value={vin}
            onChange={(e) => setVin(e.target.value)}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="customerName" className="form-label">Customer</label>
          <input
            type="text"
            id="customerName"
            className="form-control"
            value={customerName}
            onChange={(e) => setCustomerName(e.target.value)}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="dateTime" className="form-label">Date and Time</label>
          <input
            type="datetime-local"
            id="dateTime"
            className="form-control"
            value={dateTime}
            onChange={(e) => setDateTime(e.target.value)}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="technician" className="form-label">Technician</label>
          <select
            id="technician"
            className="form-control"
            value={technicianId}
            onChange={(e) => setTechnicianId(e.target.value)}
            required
          >
            <option value="">Select Technician</option>
            {technicians.map((technician) => (
              <option key={technician.id} value={technician.id}>
                {`${technician.first_name} ${technician.last_name}`}
              </option>
            ))}
          </select>
        </div>
        <div className="mb-3">
          <label htmlFor="reason" className="form-label">Reason</label>
          <input
            type="text"
            id="reason"
            className="form-control"
            value={reason}
            onChange={(e) => setReason(e.target.value)}
            required
          />
        </div>
        <button type="submit" className="btn btn-primary">Create</button>
      </form>
    </div>
  );
}

export default AppointmentsForm;
