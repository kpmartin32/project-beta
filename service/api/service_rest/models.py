from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    employee_id = models.CharField(max_length=150)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField()
    status = models.CharField(max_length=100, default="created")
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=150)
    technician = models.ForeignKey(
        "Technician",
        on_delete=models.CASCADE
    )
