from django.urls import path
from . import views


urlpatterns = [
    path("technicians/", views.api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:id>/", views.api_technician_delete, name="api_technician_delete"),
    path("appointments/", views.api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:id>/", views.api_appointment_delete, name="api_appointment_delete"),
    path("appointments/<int:id>/cancel/", views.api_appointment_cancel, name="api_appointment_cancel"),
    path("appointments/<int:id>/finish", views.api_appointment_finish, name="api_appointment_finish"),
]
